import { TennisResult } from './tennis-result';

export interface ResultProvider {
  getResult(): TennisResult;
}
