import { ResultProvider } from './result-provider';
import { TennisGame4 } from './tennis-game4';
import { TennisResult } from './tennis-result';

export class GameServer implements ResultProvider {
  private game: TennisGame4;
  private nextResult: ResultProvider;

  constructor(game: TennisGame4, nextResult: ResultProvider) {
    this.game = game;
    this.nextResult = nextResult;
  }

  getResult(): TennisResult {
    if (this.game.serverHasWon()) {
      return new TennisResult('Win for ' + this.game.server, '');
    }
    return this.nextResult.getResult();
  }
}
