import { TennisGame4 } from './tennis-game4';

export function getScore(serverScore: number, receiverScore: number): string {
  const game: TennisGame4 = new TennisGame4('player1', 'player2');
  return game.getScore(serverScore, receiverScore);
}
