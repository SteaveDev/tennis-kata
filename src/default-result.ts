import { ResultProvider } from './result-provider';
import { TennisGame4 } from './tennis-game4';
import { TennisResult } from './tennis-result';

export class DefaultResult implements ResultProvider {
  private game: TennisGame4;
  private scores: string[];

  constructor(game: TennisGame4) {
    this.scores = ['Love', 'Fifteen', 'Thirty', 'Forty'];
    this.game = game;
  }

  getResult() {
    return new TennisResult(this.scores[this.game.serverScore], this.scores[this.game.receiverScore]);
  }
}
