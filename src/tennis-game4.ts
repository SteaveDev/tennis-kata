import { Deuce } from './deuce';
import { GameServer } from './game-server';
import { GameReceiver } from './game-receiver';
import { AdvantageReceiver } from './advantage-receiver';
import { AdvantageServer } from './advantage-server';
import { DefaultResult } from './default-result';

export class TennisGame4 {
  server: string;
  receiver: string;
  serverScore: number;
  receiverScore: number;

  constructor(serverName: string, receiverName: string) {
    this.server = serverName;
    this.receiver = receiverName;
    this.serverScore = 0;
    this.receiverScore = 0;
  }

  getScore(serverScore: number, receiverScore: number): string {
    this.serverScore = serverScore;
    this.receiverScore = receiverScore;

    let result = new Deuce(
      this,
      new GameServer(
        this,
        new GameReceiver(this, new AdvantageServer(this, new AdvantageReceiver(this, new DefaultResult(this)))),
      ),
    ).getResult();
    return result.format();
  }

  receiverHasAdvantage(): boolean {
    return this.receiverScore >= 4 && this.receiverScore - this.serverScore === 1;
  }

  serverHasAdvantage(): boolean {
    return this.serverScore >= 4 && this.serverScore - this.receiverScore === 1;
  }

  receiverHasWon(): boolean {
    return this.receiverScore >= 4 && this.receiverScore - this.serverScore >= 2;
  }

  serverHasWon(): boolean {
    return this.serverScore >= 4 && this.serverScore - this.receiverScore >= 2;
  }

  isDeuce(): boolean {
    return this.serverScore >= 3 && this.receiverScore >= 3 && this.serverScore === this.receiverScore;
  }
}
