import { ResultProvider } from './result-provider';
import { TennisGame4 } from './tennis-game4';
import { TennisResult } from './tennis-result';

export class AdvantageReceiver implements ResultProvider {
  private game: TennisGame4;
  private nextResult: ResultProvider;

  constructor(game: TennisGame4, nextResult: ResultProvider) {
    this.game = game;
    this.nextResult = nextResult;
  }

  getResult(): TennisResult {
    if (this.game.receiverHasAdvantage()) {
      return new TennisResult('Advantage ' + this.game.receiver, '');
    }
    return this.nextResult.getResult();
  }
}
