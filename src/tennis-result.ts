export class TennisResult {
  serverScore: string;
  receiverScore: string;

  constructor(serverScore: string, receiverScore: string) {
    this.serverScore = serverScore;
    this.receiverScore = receiverScore;
  }

  format(): string {
    if ('' === this.receiverScore) {
      return this.serverScore;
    }
    if (this.serverScore === this.receiverScore) {
      return this.serverScore + '-All';
    }
    return this.serverScore + '-' + this.receiverScore;
  }
}
